package com.javaforever.testdemo.mock.dao;

public interface IHelloService {
	  

	   String sayHelloToSomebody(String name);
	 
}