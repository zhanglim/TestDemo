package com.javaforever.testdemo.serviceimpl;

import java.sql.Connection;
import java.util.List;

import com.javaforever.testdemo.dao.LeaveLeftDao;
import com.javaforever.testdemo.daoimpl.LeaveLeftDaoImpl;
import com.javaforever.testdemo.database.DBConf;
import com.javaforever.testdemo.domain.LeaveLeft;
import com.javaforever.testdemo.service.LeaveLeftService;


public class LeaveLeftServiceImpl implements LeaveLeftService{
	private static LeaveLeftDao instance = new LeaveLeftDaoImpl();

	@Override
	public boolean createLeaveLeft(LeaveLeft leaveLeft) throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.createLeaveLeft(connection,leaveLeft);
		}
	}

	@Override
	public boolean deleteLeaveLeft(long id) throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.deleteLeaveLeft(connection,id);
		}
	}

	@Override
	public LeaveLeft findLeaveLeftById(long id) throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.findLeaveLeftById(connection,id);
		}
	}

	@Override
	public List<LeaveLeft> getLeaveLeftListByEmpid(long empid)
			throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.getLeaveLeftListByEmpid(connection, empid);
		}
	}

	@Override
	public List<LeaveLeft> listAllLeaveLefts() throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.listAllLeaveLefts(connection);
		}
	}

	@Override
	public boolean updateLeaveLeft(LeaveLeft leaveLeft)
			throws Exception {
		try (Connection connection = DBConf.initDB()){
			return instance.updateLeaveLeft(connection,leaveLeft);
		}
	}
}
