package com.javaforever.testdemo.utils;

/** 
 * IModel description 
 *  
 * @author Jerry Shen 
 * @version v 0 Dec. 1st, 2004 
 * --------------------------------------------------------------------------- 
 * @History 
*/ 
import java.io.Serializable;  

public interface IModel extends Serializable{
}
