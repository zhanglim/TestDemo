package testng.javaforever.testdemo.daotest;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import org.testng.annotations.Test;

import com.javaforever.testdemo.dao.FineDao;
import com.javaforever.testdemo.daoimpl.FineDaoImpl;
import com.javaforever.testdemo.database.DBConf;
import com.javaforever.testdemo.domain.Fine;

import junit.framework.Assert;

/**
 * FineDaoImplTest 
 * 
 * For developer:
 * Do not add setUp and tearDown method
 * Do not use DBConf.switchToTest()
 * Do not use DBConf.switchToProduction()
 * Must extends DataSafeTestCase to protect data safe
 * @author Jerry Shen
 * @email jerry_shen_sjf@qq.com
 * 
 * @version 2.0
 *
 */
public class FineDaoImplTest {

	private static FineDao fineDao = new FineDaoImpl();
	
	public static FineDao getFineDao() {
		return fineDao;
	}

	public static void setFineDaoImpl(FineDao fineDao) {
		FineDaoImplTest.fineDao = fineDao;
	}
	
	@Test(groups = {"tests.dao"})
	public void testlistAllFine() throws Exception{
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();

		// Test
        List<Fine> fineList = getFineDao().listAllFine(con);
        Assert.assertEquals(1,fineList.size());
        Fine fine = fineList.get(0);
        Assert.assertEquals("Monthly", fine.getReason());    
        Assert.assertEquals(160208L, fine.getEmpId());

		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testGetFineListByEmpid() throws Exception{
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();

		// Test
        List<Fine> fineList = getFineDao().getFineListByEmpid(con,160208L);
        Assert.assertEquals(1,fineList.size());
        Fine fine = fineList.get(0);
        Assert.assertEquals("Monthly", fine.getReason());       
        Assert.assertEquals(160208L, fine.getEmpId());
        
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testFindFineById() throws Exception{
		
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();
		
		// Test
        Fine fine = getFineDao().findFineById(con,1);
        
        Assert.assertEquals(1,fine.getId());
        Assert.assertEquals(160208L, fine.getEmpId());
        
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testFindFineByReason() throws Exception{
		
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();
		
		// Test
        Fine fine = getFineDao().findFineByReason(con,"Monthly");
        
        Assert.assertEquals(1,fine.getId());
        Assert.assertEquals(160208L, fine.getEmpId());
        
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testUpateFine() throws Exception{		
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();
		
		// Test
        Fine fine = getFineDao().findFineById(con,1);
        
        Assert.assertEquals(1,fine.getId());
        Assert.assertEquals(160208L, fine.getEmpId());
        
        fine.setEmpId(40000L);
        fine.setUserId(4L);
        fine.setReason("Yearly");
        fine.setDescription("My second fine");
        fine.setFineBalance(new BigDecimal(3000.50));

        getFineDao().updateFine(con,fine);
        
        Fine fine1 = getFineDao().findFineById(con,1);
        

        Assert.assertEquals(1,fine1.getId());
        Assert.assertEquals(40000L, fine1.getEmpId());
        Assert.assertEquals("Yearly", fine1.getReason());
        
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testAddFineBalance() throws Exception {
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();
		
		// Test
        Fine fine = getFineDao().findFineById(con,1);
        
        Assert.assertEquals(1,fine.getId());
        Assert.assertEquals(160208L, fine.getEmpId());        
        
        Fine fine1 = getFineDao().findFineByReason(con,"Monthly");
        getFineDao().addFineBalance(con,fine1,new BigDecimal(3000));
        Fine fine2 = getFineDao().findFineByReason(con,"Monthly");

        Assert.assertEquals(1,fine2.getId());
        Assert.assertEquals(160208L, fine2.getEmpId());
        Assert.assertEquals("Monthly", fine2.getReason());
        Assert.assertEquals(new BigDecimal("3200.0045"), fine2.getFineBalance());
        
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testCreateFine() throws Exception {
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
			
		// Test
        Fine fine = new Fine();
        fine.setUserId(1L);
        fine.setEmpId(160208L);
        fine.setReason("Monthly");
        fine.setFineBalance(new BigDecimal("200.0045"));
        fine.setDescription("My first fine");    
        
        getFineDao().createFine(con,fine);
        
        Fine fine1 = getFineDao().findFineByReason(con,"Monthly");
        
        Assert.assertEquals("Monthly",fine1.getReason());
        Assert.assertEquals(160208L, fine1.getEmpId());        
 
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();
		}
	}
	
	@Test(groups = {"tests.dao"})
	public void testDeleteFine() throws Exception{
		// prepare data
		try (Connection con = DBConf.initTestDB()){
		con.prepareStatement("delete from fine;").executeUpdate();
		con.prepareStatement("insert into fine values ('1','1','160208','Monthly','200.0045','My first fine');").executeUpdate();
		// Test
        List<Fine> fineList = getFineDao().listAllFine(con);
        Assert.assertEquals(1,fineList.size());
        
        getFineDao().deleteFine(con,1L);
        
        List<Fine> fineList1 = getFineDao().listAllFine(con);
        Assert.assertEquals(0,fineList1.size());      
 
		// Clean
		con.prepareStatement("delete from fine;").executeUpdate();				
		}
	}

}
