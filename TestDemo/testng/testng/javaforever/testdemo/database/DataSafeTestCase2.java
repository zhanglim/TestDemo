package testng.javaforever.testdemo.database;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Configuration;

import com.javaforever.testdemo.database.DBConf;

/**
 * DataSafeTestCase must owned by Project Manager
 * Do not change it while you are a developer.
 * 
 * @author Jerry Shen
 * @email jerry_shen_sjf@qq.com
 *
 */
public class DataSafeTestCase2{

	public DataSafeTestCase2(){
		if (DBConf.isTestsuiteOffline() == true){ 
			Thread.currentThread().destroy();
		}
		try {
			this.setUp();
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@BeforeMethod
	@SuppressWarnings("deprecation")
	@Configuration(beforeTestMethod = true, groups = {"tests.services"})
	final protected  void setUp() throws Exception {
		DBConf.switchToTest();		
		System.out.println("Jerry::DataSafeTestCase::setup()");
	}
	
	@AfterMethod
	@SuppressWarnings("deprecation")
	@Configuration(afterTestMethod = true, groups = {"tests.services"})
	final protected void tearDown() throws Exception {
		DBConf.switchToProduction();
		System.out.println("Jerry::DataSafeTestCase::teardown()");
	}

}
